.. Monte Carlo Analysis documentation master file, created by
   sphinx-quickstart on Tue Jan  5 15:14:28 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Monte Carlo Analysis's documentation!
================================================

This python package analyses the output of a monte-carlo dropout network. The method to analyse the different uncertainty maps are described in the following article:

.. code-block:: latex

   @inproceedings{camarasa2020quantitative,
       title={{Quantitative Comparison of Monte-Carlo Dropout Uncertainty Measures for Multi-Class Segmentation}},
       author={Camarasa, Robin and Bos, Daniel and Hendrikse, Jeroen and Nederkoorn, Paul and Kooi, Eline and van der Lugt, Aad and de Bruijne, Marleen},
       note={Uncertainty for Safe Utilization of Machine Learning in Medical Imaging (UNSURE) workshop of MICCAI conference},
       year={2020}
       }
 
A talk a given at `UNSURE <https://unsuremiccai.github.io>`_ a workshop of `MICCAI <https://www.miccai2020.org/en/>`_ conference is available at this link on Youtube.

.. raw:: html

   <iframe width="560" height="315" src="https://www.youtube.com/embed/Uw2r1JRSxgE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Usage
=====

You can install this python package using pypi as follow:

.. code-block:: bash

    $ pip install monte-carlo-analysis

or using cloning the gitlab repository and installing the package locally:

.. code-block:: bash

    $ git clone https://gitlab.com/python-packages2/monte-carlo-analysis.git
    $ cd monte-carlo-analysis
    $ pip install ./ --no-cache-dir

Contribution
============

To contribute please send a merge request on the `gitlab repository <https://gitlab.com/python-packages2/monte-carlo-analysis>`_ of the project.

Contents
========

.. toctree::
   :maxdepth: 4

   modules <modules.rst>


Indices and tables
==================

* :ref:`modindex`
* :ref:`search`
