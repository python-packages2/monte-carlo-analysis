monte\_carlo\_analysis.metrics package
======================================

Submodules
----------

monte\_carlo\_analysis.metrics.AUCPRClassWiseMetric module
----------------------------------------------------------

.. automodule:: monte_carlo_analysis.metrics.AUCPRClassWiseMetric
   :members:
   :undoc-members:
   :show-inheritance:

monte\_carlo\_analysis.metrics.AUCPRMetric module
-------------------------------------------------

.. automodule:: monte_carlo_analysis.metrics.AUCPRMetric
   :members:
   :undoc-members:
   :show-inheritance:

monte\_carlo\_analysis.metrics.AUCROCMetric module
--------------------------------------------------

.. automodule:: monte_carlo_analysis.metrics.AUCROCMetric
   :members:
   :undoc-members:
   :show-inheritance:

monte\_carlo\_analysis.metrics.BRATSMetric module
-------------------------------------------------

.. automodule:: monte_carlo_analysis.metrics.BRATSMetric
   :members:
   :undoc-members:
   :show-inheritance:

monte\_carlo\_analysis.metrics.Metric module
--------------------------------------------

.. automodule:: monte_carlo_analysis.metrics.Metric
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: monte_carlo_analysis.metrics
   :members:
   :undoc-members:
   :show-inheritance:
