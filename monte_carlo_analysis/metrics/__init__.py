"""
**Author** : Robin Camarasa

**Institution** : Erasmus Medical Center

**Position** : PhD student

**Contact** : r.camarasa@erasmusmc.nl

**Date** : 2020-09-29

**Project** : monte_carlo_analysis

**Module that contains the metrics**

"""
from .Metric import Metric
from .AUCPRMetric import AUCPRMetric
from .AUCROCMetric import AUCROCMetric
from .BRATSMetric import BRATSMetric
from .AUCPRClassWiseMetric import AUCPRClassWiseMetric
