"""
**Author** : Robin Camarasa

**Institution** : Erasmus Medical Center

**Position** : PhD student

**Contact** : r.camarasa@erasmusmc.nl

**Date** : 2020-10-01

**Project** : monte_carlo_analysis

**Implement abstract class DistributionSimilarityUncertaintyMetric**

"""
from monte_carlo_analysis.uncertainty_metrics import UncertaintyMetric


class DistributionSimilarityUncertaintyMetric(UncertaintyMetric):
    """The following classes inherits from this class:

        * :mod:`monte_carlo_analysis.uncertainty_metrics.BhattacharyaCoefficentDistributionSimilarity`
        * :mod:`monte_carlo_analysis.uncertainty_metrics.KullbackLeiblerDivergenceDistributionSimilarity`
        * :mod:`monte_carlo_analysis.uncertainty_metrics.EarthMoverDistanceDistributionSimilarity`

    """
    def __init__(self):
        super(DistributionSimilarityUncertaintyMetric, self)
        self.transformation = self.get_transformation()

    def get_transformation(self) -> callable:
        """
        Define the transformation applied to a pair of distribution

        :return: Transformation to apply to the distribution
        """
        def transformation(distribution_1: np.array, distribution_2) -> float:
            pass
        return transformation

