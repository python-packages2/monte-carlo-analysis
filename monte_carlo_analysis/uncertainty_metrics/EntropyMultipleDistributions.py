"""
**Author** : Robin Camarasa

**Institution** : Erasmus Medical Center

**Position** : PhD student

**Contact** : r.camarasa@erasmusmc.nl

**Date** : 2020-10-14

**Project** : monte_carlo_analysis

**Implement class EntropyMultipleDistributions**

"""
from monte_carlo_analysis.uncertainty_metrics import MultipleDistributionsUncertaintyMetric
import numpy as np
from numba import jit


class EntropyMultipleDistributions(MultipleDistributionsUncertaintyMetric):
    """Implement EntropyMultipleDistributions. The formula applied to the family of distributions :math:`(q_{c'}(y_j|x))_{1 \leq c \leq C}` is:

    .. math::
        M^h((q_c(y_{j}|x))_{1 \leq c \leq C}) =- \sum_{c=1}^C \mathbb{E}(q_c(y_j |x)) log(\mathbb{E}(q_c(y_j |x)))

    """
    def __init__(self):
        super(EntropyMultipleDistributions, self)
        self.transformation = self.get_transformation()

    def get_transformation(self) -> callable:
        """
        Define the variance transformation applied to a family of distribution

        :return: Transformation to apply a family of distribution
        """
        @jit
        def transformation(distributions: np.array) -> float:
            mean_prediction_per_class = np.mean(distributions, axis=0)

            # Generate the log_mean_prediction_per_class taking into account
            # the case of zero means
            log_mean_prediction_per_class = mean_prediction_per_class[:]
            log_mean_prediction_per_class[
                np.where(log_mean_prediction_per_class == 0)
            ] = 1
            log_mean_prediction_per_class = np.log(log_mean_prediction_per_class)

            return - np.sum(log_mean_prediction_per_class * mean_prediction_per_class)
        return transformation
