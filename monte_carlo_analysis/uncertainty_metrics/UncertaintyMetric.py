"""
**Author** : Robin Camarasa

**Institution** : Erasmus Medical Center

**Position** : PhD student

**Contact** : r.camarasa@erasmusmc.nl

**Date** : 2020-09-29

**Project** : monte_carlo_analysis

**Implement abstract class UncertaintyMetric**

"""

class UncertaintyMetric(object):
    """
    Abstract class that implement the UncertaintyMetric.
    The following class inherit from this class:

        * :mod:`monte_carlo_analysis.uncertainty_metrics.DistributionSimilarityUncertaintyMetric`
        * :mod:`monte_carlo_analysis.uncertainty_metrics.MultipleDistributionsUncertaintyMetric`
        * :mod:`monte_carlo_analysis.uncertainty_metrics.SingleDistributionUncertaintyMetric`

    """
    def __init__(self) -> None:
        super(UncertaintyMetric, self)

    def __call__(self, *arg, **kwarg):
        """
        Compute the uncertainty metrics
        """
        pass

    def __str__(self) -> str:
        """
        To string method
        """
        return self.__class__.__name__

