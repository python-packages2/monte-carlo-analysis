"""
**Author** : Robin Camarasa

**Institution** : Erasmus Medical Center

**Position** : PhD student

**Contact** : r.camarasa@erasmusmc.nl

**Date** : 2020-10-23

**Project** : monte_carlo_analysis

** Test numba_utils functions **

"""
import numpy as np
from monte_carlo_analysis.utils.numba_utils import numba_histogram, numba_cumul_histogram


def test_numba_histogram():
    """
    Test numba_histogram function
    """
    # Define test example
    data = np.arange(5)**2
    min_value = 0
    max_value = 50
    bins = 50
    bins_edge = np.arange(51)

    # Test normalized option
    for normalized in [True, False]:
        histogram = np.zeros((50,))
        for i in range(5):
            if normalized:
                histogram[data[i]] = 0.2
            else:
                histogram[data[i]] = 1

        output_histogram, output_bins_edge = numba_histogram(
            data, bins, min_value, max_value, normalized=normalized
        )
        assert ((output_bins_edge - bins_edge) ** 2).sum() < 0.000001
        assert ((output_histogram - histogram) ** 2).sum() < 0.000001


def test_numba_cumul_histogram():
    """
    Test numba_cumul_histogram function
    """
    # Define test example
    data = np.arange(5)**2
    min_value = 0
    max_value = 50
    bins = 50
    bins_edge = np.arange(51)

    # Test normalized option
    for normalized in [True, False]:
        histogram = np.zeros((50,))
        for i in range(5):
            if normalized:
                histogram[data[i]:] += 0.2
            else:
                histogram[data[i]:] += 1

        output_histogram, output_bins_edge = numba_cumul_histogram(
            data, bins, min_value, max_value, normalized=normalized
        )
        assert ((output_bins_edge - bins_edge) ** 2).sum() < 0.000001
        assert ((output_histogram - histogram) ** 2).sum() < 0.000001
